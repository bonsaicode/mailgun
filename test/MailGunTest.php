<?php
exit;

require_once $_SERVER['APP_CONFIG'];
ini_set( 'display_errors', 1 );

# replace all emails below with valid email addresses

$htmlMessage = <<< HTML
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Title of Html Message</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>
	<body>
		Html <b>Message</b> with 2 embedded images and 2 attached images sent via mailgun.com paid account.<br/>
		<br/>
		<img src="cid:logo_email.jpg" alt="img1"/><br/>
		<br/>
		<div style="font-family:Verdana,Arial;font-size:10pt;">
			Hello, this is a test messsage.
		</div><br/>
		<br/>
		<img src="cid:logo-footer.jpg" alt="img1"/><br/>
	</body>
</html>
HTML;

$emailParams = [
	'domain'   => MAILGUN['DEFAULT_DOMAIN'],
	'sender'   => 'Surveys <surveys@yourmailgundomain.com>',
	'reply-to' => 'Test Reply <support@yourmailgundomain.com>',
	'recipients' => [
		'to'  => [
			'bob@someotherdomain1.com',
			'Fred <fred@someotherdomain2.com>'
		],
		'cc'  => [ 'Include Person <include@anotherdomain.com' ],
		'bcc' => [ 'Bcc Person <hidden@user.com>' ]
	],
	'subject' => 'Test message.',
	'body' => [
		'html' => $htmlMessage,
		'text' => 'Text message'
	],
	'attachments' => [
		PRIVATE_DIR.'/img/logo_email.jpg',
		PRIVATE_DIR.'/img/logo-footer.jpg'
	],
	'inlines' => [
		PRIVATE_DIR.'/img/logo_email.jpg',
		PRIVATE_DIR.'/logo-footer.jpg'
	],
	'options' => [
		'dkim'    => true,
		'skipSSL' => ( APP_ENV == 'localhost' ), # must skip SSL verification when running on local development machine
		'test'    => false
	]
];

header( "Content-Type: text/plain" );
$result = BonsaiCode\MailGun::sendMessage( $emailParams );
echo "result: "; print_r( $result );
