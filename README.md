# bonsaicode/mailgun

MailGun API Wrapper. Provides wrapper to easily send messages using MailGun's API including support for attachments and inline/embedded images.

# Dependencies:

You must have a mailgun account.

This class assumes a MAILGUN associative array constant is defined:
	const MAILGUN = [
		'API_KEY'        => 'your mailgun api key',
		'BASE_URL'       => 'https://api.mailgun.net/v3/',
		'DEFAULT_DOMAIN' => 'your domain name'  # your default domain to send from which can be overridden when you need to send from other domains
 	];

# Requirements
* PHP 7.1 or higher

# Installation
To install the module, run the following command in the console:

`$ composer require "bonsaicode/mailgun"`

# Usage

	see test/MailGunTest.php