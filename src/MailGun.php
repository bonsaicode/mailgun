<?php
namespace BonsaiCode;
/**
 * MailGun API Wrapper
 *
 * Provides wrapper to easily send messages using MailGun's API including support for attachments and inline/embedded images.
 *
 * Assumes this is constant associative array is already defined:
 *	const MAILGUN = [
 *		'API_KEY'        => 'your mailgun api key',
 *		'BASE_URL'       => 'https://api.mailgun.net/v3/',
 *		'DEFAULT_DOMAIN' => 'your domain name'  # your default domain to send from which can be overridden when you need to send from other domains
 *	];
 *
 * @author Alan Davis <alan@bonsaicode.dev>
 * @copyright 2018 BonsaiCode, LLC
 *
 */

class MailGun {

	/**
	 * addFile - used to add attachments and inline images to your message
	 *
	 * @param string $filename The absolute path of the file attachment, or of the image to embed into the message.
	 * @param string $attachmentName Optional. If provided, the attachment will have this name in the message instead of the original file name.
	 *
	 * @return object Returns a CURLFile object.
	 */
	private static function addFile( string $filename, string $attachmentName = null ) : object {
		$mimetype = mime_content_type( $filename );
		#echo "[$filename][$mimetype]\n"; exit;
		return new \CURLFile( $filename, $mimetype, $attachmentName );
	}

	/**
	 * sendMessage - Send a message via MailGun.  see test file for example usage
	 *
	 * @param array $params Associate array of all info needed to send the message:
	 *
	 *		[
	 *			'sender'   => Required. Who message is from, can be an email address or "Name <email>"
	 *			'subject'  => Required.  Subject of the email
	 *			'reply-to' => Optional. For setting the Reply-To message header, can be an email address or "Name <email>"
	 *			'onbehalf' => Optional. For setting the Sender message header, can be an email address or "Name <email>", to fix the "on behalf of" long string in the From field
	 *			'body' => [
	 *				'html' => Optional if 'text' is provided.  HTML version of message to send.
	 *				'text' => Optional if 'html' is provided.  Text version of message to send.
	 *			]
	 *			'recipients' => [
	 *				'to'  => Required. Array of email addresses of the recipient(s). Example: [ "bob@host.com" ], or [ "bob@host.com", "Fred <fred@host.com>" ]
	 *					   Cannot send 'cc' or 'bcc' without 'to'
	 *				'cc'  => Optional. same format as 'to'
	 *				'bcc' => Optional. same format as 'to'
	 *			],
	 *			'attachments' => Optional. Associative array where the key(s) is the absolute path of the file to attach and the value(s) is the name of the attachment as you want it to appear in the email, e.g.:
	 *				'/tmp/filename.pdf' => 'The Attachment.pdf'
	 *			'inlines' => Optional.  To inline/embed image(s) into an html message. An associative array of absolute path file names of images, e.g.: [ PRIVATE_DIR.'/lib/img/logo_email.jpg' ]
	 *				The ['body']['html'] must contain an image tag referencing the image filename with a "cid:" prefix, e.g. <img src="cid:logo_email.jpg" alt="Company Logo"/>
	 *			'options' => Optional but 'dkim' is recommended.  Array of miscellaneous options:
	 *				'dkim'    => true or false - Enables/disables DKIM signatures on per-message basis. This should ALWAYS be used and set to true.
	 *				'skipSSL' => true - Set to true when you need to skip SSL when running this on a localhost (Windows) without SSL. Defaults to false.
	 *				'test'    => true - Mailgun will accept the message but will not send it. Defaults to false.
	 *		]
	 *
	 *		# see https://documentation.mailgun.com/en/latest/api-sending.html#sending for more info
	 *
	 * @return array [
	 *			'result' => false if error occurred, associative array with mailgun results if successful,
	 *			'errno'  => if 'result' is false, this will be -1 if results from mailgun could not be json decoded, or curl error number
	 *			'error'  => if 'result' is false, this will be set to an error message
	 *		]
	 *		if 'result' is not false, then 'errno' and 'error' will not be in the output
	 */
	public static function sendMessage( array $params ) : array {

		$fields = [
			'from'    => $params['sender'],
			'subject' => $params['subject']
		];

		if( ! empty( $params['reply-to'] ) ) {
			$fields['h:Reply-To'] = $params['reply-to'];
		}
		if( ! empty( $params['onbehalf'] ) ) { # to fix the "on behalf of" long string in the From field
			$fields['h:sender'] = $params['onbehalf'];
		}
		if( isset( $params['body']['html'] ) ) {
			$fields['html'] = $params['body']['html'];
		}
		if( isset( $params['body']['text'] ) ) {
			$fields['text'] = $params['body']['text'];
		}

		foreach( $params['recipients'] as $field => $list ) {
			foreach( $list as $i => $email ) {
				$fields[$field.'['.$i.']'] = $email;
			}
		}
		if( isset( $params['attachments'] ) ) {
			$i = 0;
			foreach( $params['attachments'] as $fullPath => $attachmentName ) {
				$fields['attachment['.$i.']'] = self::addFile( $fullPath, $attachmentName );
				$i++;
			}
		}

		# to inline/embed an image
		# 1. $params['inlines'] is array of absolute path file names of images, e.g. $params['inlines'] = [ PRIVATE_DIR.'/lib/img/logo_email.jpg' ]
		# 2. $params['body']['html'] contains an image tag referencing the image filename with a "cid:" prefix, e.g. <img src="cid:logo_email.jpg" alt="Company Logo"/>
		if( isset( $params['inlines'] ) ) {
			foreach( $params['inlines'] as $i => $inline ) {
				$fields['inline['.$i.']'] = self::addFile( $inline );
			}
		}

		$skipSSL = false;
		if( isset( $params['options'] ) ) {
			if( ! empty( $params['options']['test'] ) ) {
				$fields['o:testmode'] = true;
			}
			if( ! empty( $params['options']['dkim'] ) ) {
				$fields['o:dkim'] = true;
			}
			if( ! empty( $params['options']['skipSSL'] ) ) { # have to disable when running on localhost windows only
				$skipSSL = true;
			}
		}
		#print_r( $fields ); exit;

		$output = [];
		$ch     = curl_init();
		if( $ch === false ) {
			echo "curl_init() failed\n";
		} else {
			$curlOptions = [
				CURLOPT_HTTPAUTH       => CURLAUTH_BASIC,
				CURLOPT_USERPWD        => 'api:'.MAILGUN['API_KEY'],
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_CUSTOMREQUEST  => 'POST',
				CURLOPT_URL            => MAILGUN['BASE_URL'].$params['domain'].'/messages',
				CURLOPT_POSTFIELDS     => $fields
			];

			# have to disable when running on localhost windows only
			if( $skipSSL ) {
				$curlOptions[CURLOPT_SSL_VERIFYPEER] = false;
			}

			curl_setopt_array( $ch, $curlOptions );
			$output['result'] = curl_exec( $ch ); # false or structure
			if( $output['result'] === false ) {

				$output['errno'] = curl_errno( $ch );
				$output['error'] = curl_error( $ch );

			} else { # connected to mailgun and we may or may not have been successful

				# convert returned json string to associative array
				$output['result'] = json_decode( $output['result'], true );
				if( ! isset( $output['result']['id'] ) ) {  # if no id field, then message was not sent
					$output['errno']  = -1;
					$output['error']  = $output['result']['message'];
					$output['result'] = false;
				}
			}
			curl_close( $ch );
		}
		return $output;
	}

	/**
	 * verifyWebhookSignature - to verify webhook is originating from Mailgun
	 *
	 * @param string $token
	 * @param string $timestamp
	 * @param string $signature
	 *
	 * @return boolean
	 */
	public static function verifyWebhookSignature( string $token, string $timestamp, string $signature ) : bool {
		return hash_hmac( 'sha256', $timestamp.$token, MAILGUN['API_KEY'] ) === $signature;
	}
}
